[IOT STUDENT HOME](https://gitlab.com/iot110/iot110-student/blob/master/README.md)

## Setting up Lab5

### Objectives
For this lab we will continue building on our IoT suite by creating a web app
for our BMP280 sensor device.  We will maintain our LED/Switch configuration for
now and optimize the user interface dashboard for panels of each sensing element. 
We will particularly focus on how to create data objects in one language (Python) 
and transport them (via SSE) to another language (JavaScript).  This is how our
Thing will continue to get smarter in talking to the Internet.

The various steps of this lab are summarized as:
* [PART A](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab5/PartA.md) Create a Sensor Data Object
* [PART B](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab5/PartB.md) Send the Sensor Data Object to the Webapp
* [PART C](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab5/PartC.md) Display Time, Temp and Pressure in a Bootstrap Table Structure
* [PART D](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab5/PartD.md) Add MorrisJS Charting Capability for the Pressure

[IOT STUDENT HOME](https://gitlab.com/iot110/iot110-student/blob/master/README.md)
