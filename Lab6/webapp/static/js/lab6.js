$(document).ready(function() {
    var max_data_saved = 20;
    var sse_sensor_data = [];

    iotSource.onmessage = function(e) {
        parsed_json_data = JSON.parse(e.data);
        console.log(parsed_json_data);
        parsed_json_data['meas_time'] = new Date(parsed_json_data['meas_time']);


        sse_sensor_data.unshift(parsed_json_data);
        while (sse_sensor_data.length > max_data_saved) { sse_sensor_data.pop(); }

        // Call the function that updates the environmental sensors table
        updateEnvironmentalTable();
        update_env_chart()
        updateInertiaTable()
        update_inertia_chart()
    }

    function updateEnvironmentalTable() {
        // Iterate over each environmental parameter row
        $('tr.env-param-row').each(function(i) {
            // Start new string of output html, add in measurement time
            var new_html = '<td>' + sse_sensor_data[i]["meas_time"].toLocaleString() + '</td>';
            // And add in each specific env sensor data value
            new_html += '<td>' + sse_sensor_data[i]['environmental']['temperature'].value.toFixed(2) + '</td>';
            new_html += '<td>' + sse_sensor_data[i]['environmental']['pressure'].value.toFixed(2) + '</td>';
            new_html += '<td>' + sse_sensor_data[i]['environmental']['humidity'].value.toFixed(2) + '</td>';

            // Then replace the current html with this newly created table row
            $(this).html(new_html);
        });
    }

    // In SSE OnMessage Event Handler add function call to update_env_chart()

    // ============================== ENV CHART ================================
    // initialize the accel chart structure
    var env_chart = new Morris.Line({
        element: 'env-chart',
        data: '',
        xkey: 'time',
        ykeys: ['humidity', 'temp'],
        labels: ['%RH', '&degC']
    });

    // build the environmental chart data array for MorrisJS structure
    function update_env_chart(data) {
        var chart_data = [];
        // Create chart data object to pass to morris chart object
        sse_sensor_data.forEach(function(d) {
            env_record = {
                'time': d['meas_time'].getTime(),
                'humidity': d['environmental']['humidity'].value,
                'temp': d['environmental']['temperature'].value
            };
            chart_data.push(env_record);
        });
        env_chart.setData(chart_data);
    };
    // ============================== ENV CHART ================================

    //======================INERTIA TABLE============
    function updateInertiaTable() {

        //sensors['inertial']['accelerometer'] = { 'x':accel['x'], 'y':accel['y'], 'z': accel['z'], 'unit':'g'}

        // Iterate over each inertial parameter row
        $('tr.imu-param-row').each(function(i) {
            // Start new string of output html, add in measurement time
            var new_html = '<td>' + sse_sensor_data[i]["meas_time"].toLocaleString() + '</td>';
            // And add in each specific env sensor data value
            new_html += '<td>' + sse_sensor_data[i]['inertial']['accelerometer']['x'].toFixed(2) + '</td>';
            new_html += '<td>' + sse_sensor_data[i]['inertial']['accelerometer']['y'].toFixed(2) + '</td>';
            new_html += '<td>' + sse_sensor_data[i]['inertial']['accelerometer']['z'].toFixed(2) + '</td>';


            //<tr class=param-header>
            //        <th>Time (PST)</th>
            //        <th>Accel X</th>
            //        <th>Accel Y</th>
            //        <th>Accel Z</th>
            //        <th>Pitch</th>
            //        <th>Roll</th>
            //        <th>Yaw</th>
            //        <th>Compass</th>
            //      </tr>
            //sensors['inertial']['orientation'] = { 'compass':self.sense.get_compass(), 'pitch':orientation['pitch'], 'roll':orientation['roll'], 'yaw': orientation['yaw'], 'unit':'degrees'}
            new_html += '<td>' + sse_sensor_data[i]['inertial']['orientation']['pitch'].toFixed(2) + '</td>';
            new_html += '<td>' + sse_sensor_data[i]['inertial']['orientation']['yaw'].toFixed(2) + '</td>';
            new_html += '<td>' + sse_sensor_data[i]['inertial']['orientation']['compass'].toFixed(2) + '</td>';

            // Then replace the current html with this newly created table row
            $(this).html(new_html);
        });
    }

    // ============================== Inertial CHART ================================
    // initialize the accel chart structure
    var inertia_chart = new Morris.Line({
        element: 'accel-chart',
        data: '',
        xkey: 'time',
        ykeys: ['x', 'y', 'z'],
        labels: ['Accel-X', 'Accel-Y', 'Accel-Z']
    });

    // build the environmental chart data array for MorrisJS structure
    function update_inertia_chart(data) {
        var chart_data = [];
        // Create chart data object to pass to morris chart object
        sse_sensor_data.forEach(function(d) {
            record = {
                'time': d['meas_time'].getTime(),
                'x': d['inertial']['accelerometer']['x'],  //<===  THESE ARE YOUR NEXT STEPS.  Update the data per the picture in the assignment.
                'y': d['inertial']['accelerometer']['y'],    //<=== Then add in the call to this function above.
                'z': d['inertial']['accelerometer']['z'],
            };
            chart_data.push(record);
        });
        inertia_chart.setData(chart_data);
    };
    // ============================== ENV CHART ================================
});